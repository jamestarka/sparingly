# Sparingly

Sparingly is a Java application meant to assist bowlers in improving their ability to convert spares in the game of bowling.

The name *Sparingly* is a play on the bowling term *spare* but also a word that relates to how often something occurs. The hope is that this application helps you to increase the frequency of spare conversions.

TODO: fill in the rest of this document.

## Status

This application is in its alpha stages. Most of the API is not yet determined and is subject to breaking changes. This code should not be considered production-ready; use at your own risk.

## Installation & Usage

TBD

# License

TBD

# Contributing

Contributions are welcome.