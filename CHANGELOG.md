# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [Unreleased]
### Added
- Initial project structure using Maven
- Initial functionality (scope for MVP to be determined)

[Unreleased]: https://gitlab.com/jamestarka/sparingly/compare/0.1.1...HEAD